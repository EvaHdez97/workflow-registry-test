#!/bin/bash

# Actualiza los repositorios e instala las dependencias necesarias
apt-get update && \
    apt-get install -y \
    python3 \
    python3-pip \
    libnetcdff-dev \
    libopenjp2-7-dev \
    build-essential \
    libaec-dev \
    zlib1g-dev \
    libcurl4-openssl-dev \
    libboost-dev \
    curl \
    zip \
    bzip2 \
    gfortran \
    make \
    unzip \
    git \
    cmake \
    wget \
    vim \
    nano \
    gcc \
    g++


# Instala las bibliotecas de Python y actualiza las definiciones de ECCODES
pip3 install \
    datetime \
    xarray \
    requests \
    netcdf4 \
    dtcv2-util

# Crea el directorio de datos y copia el archivo meteo.inp
mv /METEO-GFS-TEST/src/examples /METEO-GFS-TEST/
mv /METEO-GFS-TEST/src /METEO-GFS-TEST/meteo-gfs

# Crea y mueve al directorio de trabajo para descargar wgrib2
mkdir -p /METEO-GFS-TEST/wgrib2
cd /METEO-GFS-TEST/wgrib2

# Descarga el código fuente de wgrib2
wget -c ftp://ftp.cpc.ncep.noaa.gov/wd51we/wgrib2/wgrib2.tgz

# Extrae el código fuente
tar -xzvf wgrib2.tgz

# Mueve al directorio principal de grib para la compilación
cd /METEO-GFS-TEST/wgrib2/grib2

# Exporta las variables de entorno necesarias para la compilación
echo 'export CC=gcc' >> ~/.bashrc
echo 'export FC=gfortran' >> ~/.bashrc
source ~/.bashrc

# Compila y crea el binario
make

# Elimina cualquier instalación anterior de grib2 y crea el directorio nuevamente
rm -rf /usr/local/grib2/
mkdir -p /usr/local/grib2/

# Copia el binario de wgrib2 al directorio /usr/local/bin
cp -rfv ./wgrib2/wgrib2 /usr/local/bin/wgrib2

# Limpia los archivos temporales
cd /METEO-GFS-TEST
rm -rf /METEO-GFS-TEST/wgrib2
