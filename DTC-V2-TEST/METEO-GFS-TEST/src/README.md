## Purpose
This script is designed to download and process meteorological data, specifically from the GFS (Global Forecast System) model.

## Functions



- Initiates a request to download GFS data
- Checks if the required GFS files are present in the specified folder.
- If necessary, attempts to download data from the previous cycle.
- Adjusts the date and cycle for the previous time step (cycle) if cycle is not available.
- Converts GFS files in GRIB format to NetCDF format.




## Logging
The script logs various messages using a logging utility from `dtcv2_util`.


# Installation
## Dependences
To run meteo is required
- [x] Python 3.8  or higher
- [x] wgrib2

The instructions for wgrib2 library installation are in the following link https://github.com/NOAA-EMC/wgrib2

## Installing in Linux 

Can be installed using pip3 
```
pip3 install datetime  xarray requests netcdf4 
pip3 install dtcv2-util 
```

## Installing in Mac ARM64 (M1 M2 M3 Apple Chip)

For ARM64 architecture you can use CONDA or PIP

### Option 1 CONDA

For conda installation use
```
conda install  datetime  xarray requests netcdf4 dtcv2_util
```
### Option 2 PIP

You can also install the packages by using pip3
```
pip3 install datetime  xarray requests netcdf4 dtcv2_util
``` 

## How to run Meteo
The script supports python or python3.

There are three possible arguments to be used:

- **-m** : d / c / a . Mode of running, d (only Download the data), c (only Concatenates the data to create the netCDF file ) and a (do All)

- **i** : path to the input file (must follow the specifications of the meteo.inp)
- **re**: path to the resources file

If there are not parameters given, the default ones are 
- -m a
- -i ./meteo.inp
```
python meteo.py
python3 meteo.py
python3 -m a -i <path_to_inp>
```
## Configure the input data

-! PROJECT_PATH= (absolute path)  to the project path, folder Meteo will be created to download data)

-! METEO SOURCE = [GFS/ERA5/OPEN_DATA/GFES]

-! LOCATION SOURCE [MANUAL/FILE] = refers to source of area parameters. If 
source file you can use predefine json file with areas or define the path to a personal file with the same structure

-! AREA_NAME (String) =Name of the area predefined in the file area.json(resources folder). Only used if LOCATION_SOURCE =FILE 

-! AREA_FILE =(absolute Path) to file with areas

-! LON_RANGE =(min_longitude max_longitude) : range of longitudes that define the area. Only used if LOCATION_SOURCE=MANUAL 

-! LON_RANGE =(min_latitude max_latitude) : range of latitudes that define the area. Only used if LOCATION_SOURCE=MANUAL  

-!RESOLUTION= (Float) Resolution if the grid in deg

-!TIME_SOURCE= [AUTO/MANUAL] Defines if date and cycles will be given by user or just take the last available 

-!DATE= (DD/MM/YYYY): Specific date to download the data

-!CYCLE = [0/6/12/18] : hour of model start

-!TIME_STEP = [minhour maxhour] range of hours of forecast to be download

-!TIME_RESOLUTION: (Int) hourly expressed is the step in range TIME step. If hourly forecast needed, the 1

-!OUTPUT= (Str) if AUTO, files will be named with format FH-YYYYMMDD_HHz.grb, else FH-<OUTPUT>.grb

## Configuring the Resources Path

The default values and server variables for METEO-GFS are configured in the `resources` folder. Within this folder, various files in JSON format are available, each serving a specific purpose:

- **areas.json**: This file contains predefined areas that can be referenced in the input file if the corresponding option is selected.
- **default_values**: These values are selected by default in the input file when `TIME_SOURCE` is set to AUTO.
- **servers.json**: Configuration file defining download settings such as variables to download.
- **options.json**: Necessary for input file validation.


To utilize these files within the script, you must specify the path using the `-re` flag followed by the absolute path to the directory containing the `resources` folder.
