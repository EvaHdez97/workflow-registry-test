# Contribution Guidelines

Please review these contribution guidelines before making or requesting a change.

## Code of Conduct

This project adheres to a [Code of Conduct](CODE_OF_CONDUCT.md). Please review the code of conduct
before contributing.

## Creating Issues

Creating issues, whether bug reports, feature requests, implementation questions/issues, or others are
definitely encouraged. Before submitting an issue, please check for an existing issue on same subject and
comment there as appropriate rather than opening another issue. For new issues please use the `Bug` or
`Feature` description templates, if applicable, to ensure complete information is captured. If your issue
covers multiple topics, please submit a separate issue for each topic. Every attempt will be made to
respond to issues in a timely manner.

To report a security vulnerability, please see the [Security Policy](SECURITY.md).

## Contributing Code

Before submitting a merge request, especially for new features, it is advisable to discuss it in a feature
request issue first to facilitate any subsequent review.

When submitting a merge request:

- Please ensure all code and documentation follows the applicable linting rules for the project (if not run
  locally, they will be run in GitLab CI when submitted).
- Please ensure existing tests are updated, or new tests are created, as applicable for the change.
- Please ensure any applicable documentation is updated.
- Reference any applicable issues in the merge request description.

The GitLab CI pipeline will run linting and test jobs as applicable for the project. It will also run applicable
checks for vulnerabilities in dependencies, code quality, secure coding practices, and secret detection. If new
issues are identified, they should be resolved or appropriate rationale should be provided in the merge request
description.

