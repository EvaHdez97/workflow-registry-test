#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu May  4 11:45:40 2023

@author: aguerrero
'''

from dtcv2_util import log_management as logm 
import os 
import subprocess


location='NC_Functions'
nc_fil="resources/nc_default.json"
import warnings


def fxn():
    warnings.warn("deprecated", DeprecationWarning)

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    fxn()
def grib2netcdf(meteo):
    global log_file_path
    WGRIBEXE = "wgrib2"
    absFilePath = os.path.abspath(__file__)
    script_path, filename = os.path.split(absFilePath)
    
    if str(meteo.res) == "0.25":
        TABLEFILE = os.path.join(script_path,"resources/gfs_0p25.levels")
    elif str(meteo.res) == "0.50":
        TABLEFILE = os.path.join(script_path,"resources/gfs_0p50.levels")
    elif str(meteo.res) == "1.00":
        TABLEFILE =os.path.join(script_path,"resources/gfs_1p00.levels")
    else:
        logm.write_log(log_file_path, f"Unsupported resolution: {meteo.res}", "ERROR", location,"406" )
    absFilePath = os.path.abspath(__file__)
    script_path, filename = os.path.split(absFilePath)
    nc_file_path=os.path.join(script_path,nc_fil)
    log_file_path=meteo.log_file
    variables = "HGT|TMP|RH|UGRD|VGRD|VVEL|PRES|PRATE|LAND|HPBL"
    plevels = "1000|975|950|925|900|850|800|750|700|650|600|550|500|450|400|350|300|250|200|150|100|70|50|40|30|20|15|10|7|5|3|2|1|0.7|0.4|0.2|0.1|0.07|0.04|0.02|0.01"
    for i in range(meteo.time[0],meteo.time[1]+1,meteo.step):
        GRIBFILE = os.path.join(meteo.path,'{fh:03d}-{basename}'.format(basename = meteo.output,
                                                      fh       = i,
                                                     ))
        
        try:
            data_filename=os.path.join(meteo.path,'{base_name}.nc'.format(base_name=meteo.output))
            data_filename=data_filename.replace('.grb','')
            print(f"Processing {GRIBFILE}...")
            command = [
                WGRIBEXE, GRIBFILE,
                "-match", f":({variables}):",
                "-match", f":(({plevels}) mb|surface|2 m above ground|10 m above ground):",
                "-nc_table", TABLEFILE,
                "-append",
                "-nc3",
                "-netcdf",
                data_filename
            ]
            
            with open(log_file_path, 'w+') as log_file:
                
                subprocess.run(command, stdout=log_file)

            
        
            
        except Exception as e:
            logm.write_log(log_file_path, str(e), "ERROR", location,"406" )
    if os.path.isfile(data_filename):
        remove_gribs(meteo)
        logm.write_log(log_file_path, "The file is ready in"+data_filename , "SUCCESS", location,"201" )
        logm.write_log(log_file_path, "GFS completed." , "SUCCESS", location,"210" )
def remove_gribs(meteo):
    try:
        for i in range(meteo.time[0],meteo.time[1]+1,meteo.step):
            local_filename = os.path.join(meteo.path,'{fh:03d}-{basename}'.format(basename = meteo.output,
                                                      fh       = i,
                                                      ))
            os.remove(local_filename)
        for  root, dirs, files in os.walk(meteo.path):
           for currentFile in files:
               exts = ('.idx', '.idx*')
               if currentFile.lower().endswith(exts):
                   os.remove(os.path.join(meteo.path, currentFile))
    except Exception as e:
        logm.write_log(log_file_path, str(e), "ERROR", location,"406" )
        

        
